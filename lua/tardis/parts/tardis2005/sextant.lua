local PART = {}

PART.ID = "tardis2005_sextant"
PART.Name = "2005 TARDIS Sextant"
PART.Model = "models/doctorwho1200/coral/sextant.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 2.5
PART.Sound = "doctorwho1200/coral/sextant.wav"

TARDIS:AddPart(PART)