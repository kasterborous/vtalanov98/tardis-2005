local PART = {}

PART.ID = "tardis2005_radio"
PART.Name = "2005 TARDIS Radio"
PART.Model = "models/doctorwho1200/coral/audio.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)
