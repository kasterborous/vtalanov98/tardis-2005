local PART = {}

PART.ID = "tardis2005_bell"
PART.Name = "2005 TARDIS Bell"
PART.Model = "models/doctorwho1200/coral/bell.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Sound = "vtalanov98/tardis2005/bell.wav"

TARDIS:AddPart(PART)