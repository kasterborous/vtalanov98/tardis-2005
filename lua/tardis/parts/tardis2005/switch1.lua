local PART = {}

PART.ID = "tardis2005_switch1"
PART.Name = "2005 TARDIS Switch 1"
PART.Model = "models/doctorwho1200/coral/switch.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 5
PART.Sound = "doctorwho1200/coral/toggle.wav"

TARDIS:AddPart(PART)