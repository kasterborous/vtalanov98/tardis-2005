local PART = {}

PART.ID = "tardis2005_greenswitch1"
PART.Name = "2005 TARDIS Green Switch 1"
PART.Model = "models/doctorwho1200/coral/greenswitch.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/coral/plasticswitch.wav"

TARDIS:AddPart(PART)