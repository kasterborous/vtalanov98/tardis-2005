local PART = {}

PART.ID = "tardis2005_button"
PART.Name = "2005 TARDIS Button"
PART.Model = "models/doctorwho1200/coral/button.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 3

PART.Sound = "doctorwho1200/coral/button.wav"
PART.PowerOffSound = false

TARDIS:AddPart(PART)