local PART = {}

PART.ID = "tardis2005_greenswitch2"
PART.Name = "2005 TARDIS Green Switch 2"
PART.Model = "models/doctorwho1200/coral/greenswitch2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/coral/plasticswitch.wav"

TARDIS:AddPart(PART)