local PART = {}

PART.ID = "tardis2005_phone"
PART.Name = "2005 TARDIS Phone"
PART.Model = "models/doctorwho1200/coral/phone.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Sound = "doctorwho1200/coral/phone.wav"

TARDIS:AddPart(PART)