local PART = {}

PART.ID = "tardis2005_valve1"
PART.Name = "2005 TARDIS Valve 1"
PART.Model = "models/doctorwho1200/coral/valve.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctorwho1200/coral/valve.wav"

TARDIS:AddPart(PART)