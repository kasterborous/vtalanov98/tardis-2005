local PART = {}

PART.ID = "tardis2005_pipes"
PART.Name = "2005 TARDIS Pipes"
PART.Model = "models/doctorwho1200/coral/pipes.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)