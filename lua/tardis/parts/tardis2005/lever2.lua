local PART = {}

PART.ID = "tardis2005_lever2"
PART.Name = "2005 TARDIS Lever 2"
PART.Model = "models/doctorwho1200/coral/lever2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 5
PART.Sound = "doctorwho1200/coral/lever2.wav"

TARDIS:AddPart(PART)