local PART = {}

PART.ID = "tardis2005_keypad"
PART.Name = "2005 TARDIS Keypad"
PART.Model = "models/doctorwho1200/coral/keyboard.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Sound = "doctorwho1200/coral/keyboard.wav"
PART.PowerOffSound = false

TARDIS:AddPart(PART)