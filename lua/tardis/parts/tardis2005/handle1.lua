local PART = {}

PART.ID = "tardis2005_handle1"
PART.Name = "2005 TARDIS Handle 1"
PART.Model = "models/doctorwho1200/coral/handle.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctorwho1200/coral/handle.wav"

TARDIS:AddPart(PART)