local PART = {}

PART.ID = "tardis2005_regulators2"
PART.Name = "2005 TARDIS Regulators 2"
PART.Model = "models/doctorwho1200/coral/regulators2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 0.9
PART.Sound = "doctorwho1200/coral/regulators.wav"

TARDIS:AddPart(PART)