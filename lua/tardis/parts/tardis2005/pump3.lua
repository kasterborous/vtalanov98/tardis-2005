local PART = {}

PART.ID = "tardis2005_pump3"
PART.Name = "2005 TARDIS Pump 3"
PART.Model = "models/doctorwho1200/coral/pump3.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 0.4
PART.Sound = "doctorwho1200/coral/pump3.wav"

TARDIS:AddPart(PART)