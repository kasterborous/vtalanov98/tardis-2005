local PART = {}

PART.ID = "tardis2005_lever1b"
PART.Name = "2005 TARDIS Lever 1b"
PART.Model = "models/doctorwho1200/coral/lever.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 5
PART.Sound = "doctorwho1200/coral/lever.wav"

TARDIS:AddPart(PART)