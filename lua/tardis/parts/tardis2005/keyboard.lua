local PART = {}

PART.ID = "tardis2005_keyboard"
PART.Name = "2005 TARDIS Keyboard"
PART.Model = "models/doctorwho1200/coral/keyboard2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "doctorwho1200/coral/keyboard2.wav"


TARDIS:AddPart(PART)