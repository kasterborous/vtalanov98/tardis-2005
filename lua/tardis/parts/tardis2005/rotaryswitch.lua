local PART = {}

PART.ID = "tardis2005_rotaryswitch"
PART.Name = "2005 TARDIS Rotary Switch"
PART.Model = "models/doctorwho1200/coral/rotaryswitch.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctorwho1200/coral/rotaryswitch.wav"

TARDIS:AddPart(PART)