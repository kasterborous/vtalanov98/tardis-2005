local PART = {}

PART.ID = "tardis2005_redbutton"
PART.Name = "2005 TARDIS Red Button"
PART.Model = "models/doctorwho1200/coral/redbutton.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 5
PART.Sound = "doctorwho1200/coral/button.wav"

TARDIS:AddPart(PART)