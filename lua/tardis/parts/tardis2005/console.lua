local PART = {}

PART.ID = "tardis2005_console"
PART.Name = "2005 TARDIS Console"
PART.Model = "models/doctorwho1200/coral/console.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)