local PART = {}

PART.ID = "tardis2005_regulators1"
PART.Name = "2005 TARDIS Regulators 1"
PART.Model = "models/doctorwho1200/coral/regulators.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 1
PART.Sound = "doctorwho1200/coral/regulators.wav"

TARDIS:AddPart(PART)