local PART = {}

PART.ID = "tardis2005_crank1"
PART.Name = "2005 TARDIS Crank 1"
PART.Model = "models/doctorwho1200/coral/crank.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 0.8
PART.Sound = "doctorwho1200/coral/crank.wav"

TARDIS:AddPart(PART)