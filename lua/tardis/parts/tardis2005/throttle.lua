local PART = {}

PART.ID = "tardis2005_throttle"
PART.Name = "2005 TARDIS Throttle"
PART.Model = "models/doctorwho1200/coral/throttle.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 6
PART.Sound = "vtalanov98/tardis2005/lever1.wav"

TARDIS:AddPart(PART)