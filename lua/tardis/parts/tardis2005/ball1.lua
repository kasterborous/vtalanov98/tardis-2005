local PART = {}

PART.ID = "tardis2005_ball1"
PART.Name = "2005 TARDIS Ball 1"
PART.Model = "models/doctorwho1200/coral/ball.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 0.7

PART.Sound = "doctorwho1200/coral/ball.wav"
PART.PowerOffSound = false

TARDIS:AddPart(PART)