local PART = {}

PART.ID = "tardis2005_smallwheel1"
PART.Name = "2005 TARDIS Small Wheel 1"
PART.Model = "models/doctorwho1200/coral/smallwheel.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctorwho1200/coral/smallwheel.wav"

TARDIS:AddPart(PART)