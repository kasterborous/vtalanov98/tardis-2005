local PART = {}

PART.ID = "tardis2005_crank2"
PART.Name = "2005 TARDIS Crank 2"
PART.Model = "models/doctorwho1200/coral/crank2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctorwho1200/coral/crank2.wav"

TARDIS:AddPart(PART)