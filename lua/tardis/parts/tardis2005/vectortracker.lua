local PART = {}

PART.ID = "tardis2005_vectortracker"
PART.Name = "2005 TARDIS Vector Tracker"
PART.Model = "models/doctorwho1200/coral/vectortracker.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctorwho1200/coral/vectortracker.wav"

TARDIS:AddPart(PART)