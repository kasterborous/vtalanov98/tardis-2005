local PART = {}

PART.ID = "tardis2005_ball2"
PART.Name = "2005 TARDIS Ball 2"
PART.Model = "models/doctorwho1200/coral/ball2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 1

PART.Sound = "doctorwho1200/coral/ball.wav"
PART.PowerOffSound = false

TARDIS:AddPart(PART)