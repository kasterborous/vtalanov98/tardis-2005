local PART = {}

PART.ID = "tardis2005_wheel2"
PART.Name = "2005 TARDIS Wheel 2"
PART.Model = "models/doctorwho1200/coral/wheel.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 1.2
PART.Sound = "doctorwho1200/coral/wheel.wav"

TARDIS:AddPart(PART)