local PART = {}

PART.ID = "tardis2005_lever3"
PART.Name = "2005 TARDIS Lever 3"
PART.Model = "models/doctorwho1200/coral/lever3.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 3
PART.SoundOn = "doctorwho1200/coral/lever3on.wav"
PART.SoundOff = "doctorwho1200/coral/lever3off.wav"


TARDIS:AddPart(PART)