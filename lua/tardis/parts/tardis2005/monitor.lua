local PART = {}

PART.ID = "tardis2005_monitor"
PART.Name = "2005 TARDIS Monitor"
PART.Model = "models/doctorwho1200/coral/monitor.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)