local PART = {}

PART.ID = "tardis2005_switch2"
PART.Name = "2005 TARDIS Switch 2"
PART.Model = "models/doctorwho1200/coral/switch2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctorwho1200/coral/toggle.wav"

TARDIS:AddPart(PART)