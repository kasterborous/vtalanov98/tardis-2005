local PART = {}

PART.ID = "tardis2005_pump1"
PART.Name = "2005 TARDIS Pump 1"
PART.Model = "models/doctorwho1200/coral/pump.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 0.38
PART.Sound = "doctorwho1200/coral/pump4.wav"

TARDIS:AddPart(PART)